import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:getnews/Models/News.dart';

enum NewsBlocState { loading, loaded }

class NewsBloc {
  Firestore _firestore = Firestore.instance;
  NewsBlocState _state = NewsBlocState.loading;
  StreamController<List<News>> _controller = StreamController();

  NewsBloc() {
    getNews().then((value) {
      _controller.add(value);
      _state = NewsBlocState.loaded;
    });
  }

  Future<List<News>> getNews() async {
    QuerySnapshot _docs = await _firestore.collection('news').getDocuments();
    return _docs.documents.map((e) {
      Image _photo = Image.network(
        e.data['photoURL'],
        fit: BoxFit.cover,
      );
      return News(
        uid: e.documentID,
        title: e.data['title'],
        content: e.data['content'],
        image: _photo,
      );
    }).toList();
  }

  Future<News> getPost(String uid) async {
    DocumentSnapshot _doc = await _firestore.document('news/$uid').get();
    Image _photo = Image.network(
      _doc.data['photoURL'],
      fit: BoxFit.cover,
    );

    return News(
      title: _doc.data['title'],
      content: _doc.data['content'],
      image: _photo,
    );
  }

  NewsBlocState get state => _state;
  Stream<List<News>> get stream => _controller.stream;

  void dispose() {
    _controller.close();
  }
}
