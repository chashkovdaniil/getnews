import 'package:flutter/material.dart';
import 'package:getnews/Models/News.dart';
import 'package:getnews/Bloc/NewsBloc.dart';
import 'package:getnews/TimeArticles.dart';

class NewsPage extends StatefulWidget {
  final String uid;
  NewsPage({Key key, this.uid}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  News _post;
  NewsBloc _nBloc = NewsBloc();

  @override
  void initState() {
    _nBloc.getPost(widget.uid).then((value) {
      setState(() {
        _post = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _post == null
          ? Center(child: CircularProgressIndicator())
          : ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    _post.title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    width: double.infinity,
                    height: 150,
                    color: Colors.grey,
                    child: _post.image,
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    'Барабулька барабулькович',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
                Text(_post.content),
              ],
            ),
    );
  }
}
