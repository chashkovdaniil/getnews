
import 'package:flutter/material.dart';

class News{
  String uid;
  String title;
  String content;
  Image image;

  News({
    this.uid,
    this.title,
    this.content,
    this.image,
  });
}