import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:getnews/Models/News.dart';
import 'package:getnews/News.dart';
import 'package:getnews/Bloc/NewsBloc.dart';
import 'package:getnews/TimeArticles.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        appBarTheme:
            AppBarTheme(color: Theme.of(context).scaffoldBackgroundColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: 1.0,
          ),
        );
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _navigatorKeys = [
    GlobalKey<NavigatorState>(debugLabel: 'news'),
    GlobalKey<NavigatorState>(debugLabel: 'notNews'),
  ];
  int _currentPage = 0;
  List<Widget> _pages;
  final NewsBloc _nBloc = NewsBloc();

  @override
  void initState() {
    _pages = [
      WillPopScope(
        onWillPop: () async {
          if (Navigator.of(context).canPop()) {
            log('here1');
            return false;
            // Navigator.of(context).pop();
          }
          if (_navigatorKeys[0].currentState.canPop()) {
            _navigatorKeys[0].currentState.pop();
            log('here2');
            return false;
          }
          log('here3');
          return false;
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Divider(
              height: 1,
              color: Colors.black87,
            ),
            TimeArticles(),
            Expanded(
              child: Navigator(
                key: _navigatorKeys[0],
                onGenerateRoute: (RouteSettings _rs) {
                  return MaterialPageRoute(
                    builder: (_) => ListNews(bloc: _nBloc),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      WillPopScope(
        onWillPop: () async {
          if (Navigator.of(context).canPop()) {
            Navigator.of(context).pop();
            return false;
          }
          if (_navigatorKeys[1].currentState.canPop()) {
            _navigatorKeys[1].currentState.pop();
            return false;
          }
          return true;
        },
        child: Navigator(
          key: _navigatorKeys[1],
          onGenerateRoute: (RouteSettings _rs) {
            return MaterialPageRoute(
              builder: (_) => Container(
                color: Colors.red,
                child: Center(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => Scaffold(
                            appBar: AppBar(
                              title: Text('sdf'),
                            ),
                          ),
                        ),
                      );
                    },
                    child: Text('bruh'),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: GlobalKey<ScaffoldState>(),
      appBar: AppBar(
        title: Text('Новости'),
        centerTitle: true,
        elevation: 0,
      ),
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.green.withOpacity(0.3),
        currentIndex: _currentPage,
        onTap: (value) {
          setState(() {
            _currentPage = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.title),
            title: const SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tonality),
            title: const SizedBox.shrink(),
          ),
        ],
      ),
      body: IndexedStack(
        index: _currentPage,
        children: _pages,
      ),
    );
  }
}

class ListNews extends StatelessWidget {
  NewsBloc bloc;
  ListNews({Key key, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<News>>(
      initialData: [],
      stream: bloc.stream,
      builder: (context, snapshot) {
        if (bloc.state == NewsBlocState.loading) {
          return Center(child: CircularProgressIndicator());
        }
        return ListView.builder(
          shrinkWrap: true,
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
          ),
          // physics: const NeverScrollableScrollPhysics(),
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
              ),
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => NewsPage(
                        uid: snapshot.data[index].uid,
                      ),
                    ),
                  );
                },
                child: Ink(
                  height: 120,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 20,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.6),
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Container(
                          width: 80,
                          height: 80,
                          color: Colors.grey,
                          child: snapshot.data[index].image,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              snapshot.data[index].title,
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              snapshot.data[index].content,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 13,
                              ),
                            ),
                            const Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Admin',
                                  style: const TextStyle(
                                    fontSize: 10,
                                  ),
                                ),
                                Text(
                                  '10:40',
                                  style: const TextStyle(
                                    fontSize: 10,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
