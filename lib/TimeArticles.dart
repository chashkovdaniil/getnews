import 'package:flutter/material.dart';

class TimeArticles extends StatelessWidget {
  const TimeArticles({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 15,
      ),
      child: Row(
        children: [
          Text('Время статей'),
          const Spacer(),
          Text('Вчера'),
          Container(
            padding: const EdgeInsets.all(4),
            margin: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            decoration: BoxDecoration(
              color: Colors.amberAccent.shade700,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Text('Сегодня'),
          ),
          Text('Завтра'),
        ],
      ),
    );
  }
}
